# tex-template, a cli tool for making LaTeX templates

## Usage

usage: tex-template.py [-h] [--title [title]] [--author [author]] [--bib]
                       [--toc | --no-toc]

Outputs a LaTeX template to standard out.

optional arguments:
  -h, --help            show this help message and exit
  --title [title], -t [title]
                        the title of your LaTeX document.
  --author [author], -a [author]
                        the author of your LaTeX document. (you!)
  --bib, -b             adds a bibliography
  --toc                 adds a table of contents (default).
  --no-toc              doesn't include a table of contents
