#!/usr/bin/env python

import tex_template
import sys, argparse

parser = argparse.ArgumentParser(
    description="Outputs a LaTeX template to standard out."
)

parser.add_argument(
    '--title', '-t',
    metavar='[title]', type=str,
    default='<++>',
    help='the title of your LaTeX document.'
)

parser.add_argument(
    '--author', '-a',
    metavar='[author]', type=str,
    default='<++>',
    help='the author of your LaTeX document. (you!)'
)

parser.add_argument(
    '--bib', '-b', action='store_true',
    help='adds a bibliography'
)

parser.add_argument(
    '--scratchpad', '-s', action='store_true',
    help='only the content, and nothing else (good for math equations).'
)

parser.add_argument(
    '--verbose', '-v', action='store_true',
    help='verbose output (to standard err)'
)

toc_parser = parser.add_mutually_exclusive_group(required=False)
toc_parser.add_argument(
    '--toc',    dest='toc', action='store_true',
    help='adds a table of contents (default).')
toc_parser.add_argument('--no-toc', dest='toc', action='store_false',
    help="doesn't include a table of contents")
parser.set_defaults(toc=True)

args = parser.parse_args()

if args.verbose:
    print(args, file=sys.stderr)

print(tex_template.main(args=args))
