from setuptools import setup

setup(
    name='tex_template',
    version='0.1',
    description='Generates as latex document',
    scripts=['bin/tex-template'],
    url='https://gitlab.com/CoffeeVector/tex-template',
    author='Kevin Zheng',
    author_email='coffeevector@gmail.com',
    license='MIT',
    packages=['tex_template'],
    zip_safe=False,
    include_package_data=True
)
