from jinja2 import Environment, PackageLoader

import sys, argparse

env = Environment(
    block_start_string = '\BLOCK{',
    block_end_string = '}',
    variable_start_string = '\VAR{',
    variable_end_string = '}',
    comment_start_string = '\#{',
    comment_end_string = '}',
    line_statement_prefix = '%%',
    line_comment_prefix = '%#',
    trim_blocks = True,
    lstrip_blocks = True,
    autoescape = False,
    loader = PackageLoader("tex_template")
)

def main(args=None):
    paper_template = env.get_template('tex_template.tex.jinja')
    return paper_template.render(args=args)
